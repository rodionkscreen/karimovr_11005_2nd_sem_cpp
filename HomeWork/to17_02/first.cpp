#include <iostream>
using namespace std;

int power(int x, int p){
    int rez = 1;
    for (int i = 0; i < p; i++){
        rez = rez * x;
    }
    return rez;
}


int main(){
cout << "Hello_world!_" << endl;
int x = 0; int y = 0;
// cin >> x >> y;
// cout << x << y << endl;

    //1
    cout << "Task №1" << endl;
    int sum = 0;
    // size_t size;
    int size1 = 0;
    cout << "Write count of numbers" << endl;
    cin >> size1;
    int const size = size1;
    int* array = new int[size];
    for (int i = 0; i < size; i++){
        cout << "Write number " << i << ": " << endl;
        cin >> array[i];
        sum += array[i];
    }
    cout << "Sum: " << sum << endl;

    //2
    cout << "Task №2" << endl;
    int a = 0; int b = 0; int c = 0;
    cout << "Write a, b ,c: " << endl;
    cin >> a >> b >> c;
    int d = b * b - 4*a*c;
    int x1 = (-b + sqrt(d))/(2*a);
    int x2 = (-b - sqrt(d))/(2*a);
    cout << x1 << " " << x2 << endl;

//3
cout << "Task №3" << endl;
x = 0;
cout << "Write x: " << endl;
cin >> x;
int i = 0;
while (i*i < x){
    i++;
}
cout << i << endl;

//4
cout << "Task №4" << endl;
int p = 0;
cout << "Write x & p: " << endl;
cin >> x >> p;
int rez = power(x, p);
cout << rez << endl;
}
