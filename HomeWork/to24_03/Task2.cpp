#include <iostream>
using namespace std;
#include "HomeWork/to24_03/BinaryTree.cpp"
int* Sort(int *arr){
    
    BinaryTree *binaryTree = new BinaryTree();

    for(int i = 0; i < sizeof(arr); i++){
        binaryTree->Insert(arr[i]);
    }

    int *sortedArr = new int[sizeof(arr)];
    for (int i = sizeof(arr); i > 0;  i--){
        sortedArr[i] = binaryTree->ExactMax();
    }

    return sortedArr;
}

int main(){
    int * arr = new int[5];
    arr[0] = 4;
    arr[1] = 2;
    arr[2] = 5;
    arr[3] = 8;
    arr[4] = 1;

    int *sortedArr = Sort(arr);

    for (int i = 0; i < sizeof(sortedArr); i++){
        cout << sortedArr[i] << endl;
    }
}