#include <iostream>
using namespace std;;
#include <string>

struct BinaryTree{
    int size = 0;
    int maxSize = 1000;
    int array[999];
    int result = array[0];

    void swap(int *a, int *b){
        int temp = *a;
        *a = *b;
        *b = temp;
    }

    int Parent(int i){
        //return array[(i-1)/2];
        return (i-1)/2;
    }
    int LeftChild(int i){
        //return array[2*i + 1];
        return 2*i + 1;
    }
    int RightChild(int i){
        //return array[2*i + 2];
        return 2*i + 2;
    }

    void SftUp(int i){
        while (i > 0 && array[i] > array[Parent(i)]){
            swap (&array[i], &array[Parent(i)]);
            i = Parent(i);
        }
    }
    void SiftDown(int i){
        while (i < size && array[i] < array[LeftChild(i)] && array[i] < array[RightChild(i)]) {
            if (array[LeftChild(i)] > array[RightChild(i)]){
            swap(&array[i], &array[LeftChild(i)]);}
            else swap(&array[i], &array[RightChild(i)]);
            
        }

    }

    void Insert(int p){
        if (size < maxSize){
            array[size] = p;
            SftUp(size);
            size++;
        }
    }

    int GetMax(){
        return array[0];
    }
    int ExactMax(){
        int res = array[0];
        array[0] = array[size - 1];
        size--;
        SiftDown(0);
        return res;
    }


};

int main(){
    BinaryTree * binaryTree = new BinaryTree();
    binaryTree->Insert(5);
    binaryTree->Insert(3);
    binaryTree->Insert(2);
    binaryTree->Insert(4);
    binaryTree->Insert(6);
    binaryTree->Insert(8);
    
    for (int i = 0; i < binaryTree->size; i++){
        cout << binaryTree->array[i] << endl;
    }

    int maxElem = binaryTree->GetMax();
    cout << endl << maxElem << endl;
    maxElem = binaryTree->ExactMax();
    cout << maxElem << endl << endl;

    for (int i = 0; i < (*binaryTree).size; i++){
        cout << binaryTree->array[i] << endl;
    }

}