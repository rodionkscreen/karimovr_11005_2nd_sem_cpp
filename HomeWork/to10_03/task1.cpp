#include <iostream>
using namespace std;

struct Node
{
    int item;
    Node * next;
    Node * prev;
};

 
struct LinkedList
{
    Node * pHead;
    Node * pRear;

    void InsertAt(int item, int id)
    {
        
        // Node * node = new Node;
        Node * temp = pHead;
        if (pHead == nullptr)
        pHead = temp;
        int tempCounter = 0;
        while (tempCounter < id){
            tempCounter++;
            temp = temp->next;
        }
        Node *temp1 = new Node;
        temp1->item = item;
        temp1->next = temp->next;
        temp1->prev = temp;
        temp->next = temp1;
        temp = temp->next;
        temp->prev = temp1;
        
        // Last Node
        while (tempCounter < id){
            temp = temp->next;
        }
        pRear = temp;
    }

    void RemoveAt(int id){
        Node *temp = pHead;
        int counter = 0;
        while (counter < id - 1){
            counter++;
            temp = temp->next;
        }
        Node *temp1 = temp->next;
        Node *temp2 = temp1->next;
        delete temp1;
        temp->next = temp2;
        temp2->prev = temp;
    }

    void Add(int item)
    {
        Node * temp = pHead;
        while (temp->next != nullptr){
            temp = temp->next;
        }
        temp->item = item;
        temp->next = pHead;
        pHead = temp;
    }

    int size(){
        Node * temp = pHead;
        int count = 0;
        while (temp->next != nullptr){
            count++;
            temp = temp->next;
        }
        return count;
    }

    Node getNode(int id){
        Node * temp = pHead;
        int count = 0;
        while (count < id){
            count++;
            temp = temp->next;
        }
        return *temp;
    }

    int Pop()
    {
        if (!pHead)
            throw 1;

        Node * temp = pHead;
        int x = temp->item;
        pHead = temp->next;
        delete temp;
        return x;
    }

    int Peek()
    {
        if (!pHead)
            throw 1;

        return pHead->item;
    }

    void printAll(){
        Node *temp = pHead;
        while (temp != nullptr){
            cout << temp->item << endl;
            temp = temp->next;
        }
        
    }
};

int main(){


}