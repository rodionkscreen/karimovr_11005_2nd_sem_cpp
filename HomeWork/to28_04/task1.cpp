#include <iostream>
#include <vector>
using namespace std;; //P
//Задание 1.
//На вход подаются три числа A, B, C. Нужно сказать, можно ли переставить цифры в числах А и В местами так, чтобы в сумме получалось число С.
//Например, входные значения: 12, 31, 25. Ответ: 12, 13. (в сумме 25).

void sort(int arr[], int length){
for (int i = 0; i < length - 1; i++){
    for (int j = i; j < length; j++)
        if (arr[i] > arr[j]){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        }
    }
}

void add(int *a1, int aLength, vector<int> &ARes){ // В вектор добавляется число из a1
    int res = 0; int temp = 1;
    for (int i = 0; i < aLength; i++){
        res = res + a1[i] * temp;
        temp *= 10;
    }
    ARes.push_back(res);
        // cout << res << endl;
        // cout << ARes.at(0);
}

void swap(int *a, int i, int j)
{
  int s = a[i];
  a[i] = a[j];
  a[j] = s;
}

bool Next(int *a1, int aLength){
    int j = aLength - 2;
    while (j != -1 && a1[j] >= a1[j+1]) j--;
    if (j == -1) return false;
    int k = aLength - 1;
    while (a1[j] >= a1[k]) k--;
    swap(a1, j, k);
    int l = j + 1; int r = aLength - 1;
    while (l < r) swap (a1, l++, r--);
    
    return true;
}

bool Find(vector<int> ARes, vector<int> BRes, int c){
    for (int i = 0; i < ARes.size(); i++){
        for (int j = 0; j < BRes.size(); j++){
            int temp1 = ARes.at(i);
            int temp2 = BRes.at(j);
            if (temp1 + temp2 == c){
                return true;
            }
        }
    } return false;
}

int main(){
    int a; int b; int c;
    // cout << "Write a, b, c: ";
    // cin >> a >> b >> c;
    a = 13; b = 13; c = 25;
int aLength = 0;
int bLength = 0;
    vector <int> A;
    while (a != 0){
        A.push_back(a % 10);
        a = a / 10;
        aLength++;
    }
    vector <int> B;
    while (b != 0){
        B.push_back(b % 10);
        b = b / 10;
        bLength++;
    }


int *a1 = new int[aLength];
for (int i = 0; i < aLength; i++){
    a1[i] = A.at(i);
} sort(a1, aLength);
int *b1 = new int[bLength];
for (int i = 0; i < bLength; i++){
    b1[i] = B.at(i);
} sort(b1, bLength);
// cout <<"a1: " << a1[0] << " " << a1[1] << " b1: " << b1[0] << " " << b1[1] << endl;

vector<int> ARes;
add(a1, aLength, ARes);

while (Next(a1, aLength)){
    // add(a1, aLength, ARes);
    add (a1, aLength, ARes);
    // cout << "Adding to vector.."<< endl;
}

vector<int> BRes;
add(b1, bLength, BRes);
while (Next(b1, bLength)){
    add (b1, bLength, BRes);
}

// cout << "\n";
// cout<<"FIND"<< endl;
for (int i = 0; i < ARes.size(); i++){
    cout<< ARes.at(i) << " ";
} cout << endl;

for (int i = 0; i < BRes.size(); i++){
    cout<< BRes.at(i) << " ";
} cout << endl;

// cout << ARes.at(0);

cout<< "Res: " << Find(ARes, BRes, c);
}
