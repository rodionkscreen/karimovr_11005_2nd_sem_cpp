#include <iostream>
#include <iostream>
using namespace std; //P
//Задание 3.
// Мы находимся в левой нижней клетке двумерного массива NxM и можем шагать только на 1 клетку вверх, 
// или на 1 клетку вправо (влево и вниз - нельзя). Нужно посчитать, сколько возможных путей из левой 
// нижней клетки до правой верхней. На вход подаются два числа - N и M. На выход одно число - количество путей.

int main(){
    int a; int b;
    cout << "Write a, b: " << endl;
    cin >> a >> b;

    int ** a1 = new int*[a]; 
    for (int i = 0; i < a; i++){ // 0..a-1
        a1[i] = new int[b];
    }

    // int **arr = new int*[a];
    // arr[0] = new int[a * b];
    // for (int i = 1; i < a; i++){
    //     arr[i] = arr[i + 1] + b;
    // }
    
    for (int i = 0; i < b; i++){
        a1[a-1][i] = 1;
    }
    for (int i = 0; i < a; i++){
        a1[i][0] = 1;
    }
    for (int i = a-2; i > -1; i--){
        for (int j = 1; j < b; j++){
            a1[i][j] = a1[i+1][j] + a1[i][j-1];
        }
    }

    int res = a1[0][b-1];
    cout << "Answer: ";
    cout  << res;
}
