#include <iostream>
using namespace std;; //P
//Задание 2.
// Мы находимся в первой (нулевой) ячейке массива и можем шагать на 1 или на 2 клетки вперёд. Нужно посчитать, 
//сколько различных путей из первой клетки в последнюю. На вход подаётся одно число - длина массива.
// На выход одно число - количество путей.
int grasshoper(int a){
    int a1 = 0;
    int a2 = 0;
    // int a3 = 0;
    int res = 1;
    int countCells = 1;
    while (countCells != a){
        a2 = res;
        res = res + a1;
        a1 = a2;
        countCells++;
    }
    return res;
}

int main(){
int a;
cin >> a;
cout << grasshoper(a);
}
