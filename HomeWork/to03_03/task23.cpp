#include <iostream>
using namespace std;

struct Node
{
    int item;
    Node * next;
};

 
struct LinkedList
{
    Node * pHead;

    void Add(int item)
    {
        Node * node = new Node;
        Node * temp = pHead;
        while (temp->next != nullptr){
            temp = temp->next;
        }
        temp->item = item;
        temp->next = pHead;
        pHead = temp;
    }

    int size(){
        Node * temp = pHead;
        int count = 0;
        while (temp->next != nullptr){
            count++;
            temp = temp->next;
        }
        return count;
    }

    Node getNode(int id){
        Node * temp = pHead;
        int count = 0;
        while (count < id){
            count++;
            temp = temp->next;
        }
        return *temp;
    }

    int Pop()
    {
        if (!pHead)
            throw 1;

        Node * temp = pHead;
        int x = temp->item;
        pHead = temp->next;
        delete temp;
        return x;
    }

    int Peek()
    {
        if (!pHead)
            throw 1;

        return pHead->item;
    }

    void printAll(){
        Node *temp = pHead;
        while (temp != nullptr){
            cout << temp->item << endl;
            temp = temp->next;
        }
        
    }
};

int main(){
    LinkedList * linkedList = new LinkedList();
    linkedList->Add(1);
    linkedList->Add(2);
    linkedList->printAll();
    cout << (linkedList->getNode(1))->next << endl;
    cout << linkedList->size() << endl;

}
