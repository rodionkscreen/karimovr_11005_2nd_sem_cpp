#include <iostream>
using namespace std;

struct Node
{
    int item;
    Node * next;
};

 
struct Stack
{
    Node * pHead;

    void Push(int item)
    {
        Node * node = new Node;
        node->item = item;
        node->next = pHead;
        pHead = node;
    }

    int Pop()
    {
        if (!pHead)
            throw 1;

        Node * temp = pHead;
        int x = temp->item;
        pHead = temp->next;
        delete temp;
        return x;
    }

    int Peek()
    {
        if (!pHead)
            throw 1;

        return pHead->item;
    }

    void printAll(){
        Node *temp = pHead;
        while (temp != nullptr){
            cout << temp->item << endl;
            temp = temp->next;
        }
        
    }
};

int main(){
    Stack * stack = new Stack();
    stack->Push(1);
    stack->Push(2);
    stack->printAll();

}