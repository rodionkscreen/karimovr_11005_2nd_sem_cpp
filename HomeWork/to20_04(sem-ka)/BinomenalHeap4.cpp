#include <iostream>
#include <vector>
#include <list>
#include <ctime>
#include <fstream>
#include <string>
using namespace std;

struct Node{
    int data, degree;
    Node *son, *bro, *parent;
};

Node* newNode(int key){
    Node *temp = new Node;
    temp->data = key;
    temp->bro = temp->son = temp->parent = nullptr;
    return temp;
}

Node* mergeTrees(Node* t1, Node* t2){
    if(t1->data > t2->data)
    swap(t1, t2); // теперь t1 < t2
    t2->parent = t1;
    t2->bro = t1->son;
    t1->son = t2;
    t1->degree++;
    return t1;
}

list<Node*> unionHeaps(list<Node*> h1, list<Node*> h2){
    list<Node*> _new;
    list<Node*>::iterator it1 = h1.begin();
    list<Node*>::iterator it2 = h2.begin();
    while(it1 != h1.end() && it2 != h2.end()){
        if((*it1)->degree <= (*it2)->degree){
            _new.push_back(*it1);
            it1++;
        } else{
            _new.push_back(*it2);
            it2++;
        }
    } 
    while(it1 != h1.end()){
        _new.push_back(*it1);
        it1++;
    }
    while(it2 != h2.end()){
        _new.push_back(*it2);
        it2++;
    } // Все деревья добавлены в упорядоченном порядке
    return _new;    
}

list<Node*> siftHeap(list<Node*> _heap){
    if(_heap.size() <= 1)
    return _heap;
    list<Node*> newheap;
    list<Node*>::iterator it1, it2, it3;
    it1 = it2 = it3 = _heap.begin();

    // if(_heap.size() == 2){
    //     it2 = it1;
    //     it2++;
    //     it3 = _heap.end();
    // } else{
        it2++;
        it3 = it2;
        it3++;
    // }
    while(it1 != _heap.end()){
        if(it2 == _heap.end()){
            it1++;
        } else if((*it1)->degree < (*it2)->degree){
            it1++;
            it2++;
            if(it3 != _heap.end())
            it3++;
        } else if(it3 != _heap.end() && (*it1)->degree == (*it2)->degree && (*it2)->degree == (*it3)->degree){
            it1++; it2++; it3++;
        } else if((*it1)->degree == (*it2)->degree){
            Node *temp;
            *it1 = mergeTrees(*it1, *it2);
            it2 = _heap.erase(it2);
            if(it3 != _heap.end())
                it3++;
        }
    }
    return _heap;
}

list<Node*> insertTreeInHeap(list<Node*> _heap, Node *tree){
    list<Node*> temp;
    temp.push_back(tree);
    temp = unionHeaps(_heap, temp);
    return siftHeap(temp);
}

list<Node*> removeMin(Node *tree){
    list<Node*> heap;
    Node *temp = tree->son;
    Node *temp1;
    while(temp){
        temp1 = temp;
        temp = temp->bro;
        temp1->bro = nullptr;
        heap.push_front(temp1);
    }
    return heap;
}

list<Node*> insert(list<Node*> head, int key){ //
    Node *temp = newNode(key);
    return insertTreeInHeap(head, temp); //
}

Node* getMin(list<Node*> heap){ //
    list<Node*>::iterator it = heap.begin(); //
    Node *temp = *it;
    while(it != heap.end()){ //
        if((*it)->data < temp->data)
        temp = *it;
        it++;
    }
    return temp;
}

list<Node*> extractMin(list<Node*> heap){ //
    list<Node*> newHeap, temp1;
    Node *temp; // указатель мин знач
    temp = getMin(heap); //
    list<Node*>::iterator it;
    it = heap.begin(); //
    while(it != heap.end()){ //
        if(*it != temp){
        newHeap.push_back(*it);
        }
    it++;
    }
    temp1 = removeMin(temp);
    newHeap = unionHeaps(newHeap, temp1);
    newHeap = siftHeap(newHeap);
    return newHeap;
}

void printTree(Node *tree){
    while(tree){
        cout << tree->data << " ";
        printTree(tree->son);
        tree = tree->bro;
    }
}
void printHeap(list<Node*> heap){ //
    list<Node*>::iterator iterator;
    iterator = heap.begin(); //
    while(iterator != heap.end()){
        printTree(*iterator);
        iterator++;
    }
}

int main(){
        int a, key;
        list <Node*> :: iterator it; 
        list<Node*> heap;
        it = heap.begin();
    double startTime = clock();
    ifstream fin("data5000.txt");
    while (!fin.eof())
    {
        int s;
        fin >> s;

        heap.insert(heap, s);
    }
    fin.close();
    
    while(heap.size() >= 1){
        heap = extractMin(heap);

    }
    double endTime = clock();
    double totalTime = endTime - startTime;
    cout << totalTime << endl;


    // int a, key;
    // list<Node*> heap;
    // heap.insert(heap,1);
    // heap.insert(heap,2);
    // heap.insert(heap,3);
    // printHeap(heap);

    // Node *temp = getMin(heap); //
    // cout << temp->data << endl;

    // heap = extractMin(heap);//
    // printHeap(heap);

}
