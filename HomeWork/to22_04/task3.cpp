#include <iostream>
using namespace std; //Р
//Найти округлённое до целых чисел значение квадратного корня из числа Х (использовать бинарный поиск по ответу). 
//Написать функцию, которая принимает на вход натуральное число Х и возвращает приближённое значение корня.

int sqr(int x){
    return x*x;
}
int sqrt2(int *arr, int start, int end, int find){
    int tempRes = arr[(start + end)/2];
    if (tempRes*tempRes == find){
        return tempRes;
    }
    if (sqr(tempRes) < find && sqr(tempRes + 1) > find){
        return tempRes; 
    }
    if (sqr(tempRes) > find && sqr(tempRes - 1) < find){
        return tempRes - 1; 
    }
    
    if (tempRes*tempRes > find){
        sqrt2(arr, start, (start + end)/2, find);
    }
    if (tempRes*tempRes < find){
        sqrt2(arr, (start + end)/2, end, find);
    }

}
int sqrt1(int x){
    int *arr = new int[x];
    for (int i = 0; i < x; i++){
        arr[i] = i;
    }
    // int num = binSearch(arr, 0, x, x);
    int start = 0; int end = x - 1; int res;

    res = sqrt2(arr, start, end, x);
    return res;
    
}

int main(){
    int x;
cin >>  x;
int res = sqrt(x);
cout << res;
}
