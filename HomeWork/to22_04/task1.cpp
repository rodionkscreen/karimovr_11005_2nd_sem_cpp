#include <iostream>

using namespace std; // Работает

int binSearch(int *arr, int start, int end, int find){
    int middle = (start + end)/2;
    int temp  = *(arr + middle);
    if (temp == find){
        while (temp == find){
            middle--;
            temp = *(arr + middle);
        }
        middle++;
        int a1 = middle; temp = *(arr + a1);
        return middle;

    } 
    if (temp > find){
        binSearch(arr, start, middle, find);
    }
    if (temp < find){
        binSearch(arr, middle, end, find);
    }
}

int main(){
    int arr[] = {1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 5, 5, 5, 5, 5, 9, 9, 9, 9, 9, 9};
    int size = sizeof(arr)/sizeof(int);  //22 
    int *a = arr;
    int a1 = binSearch(a, 0, size, 2);
    int a2 = binSearch(a, 0, size, 3) - 1;
    cout << a1 << " " << a2 << endl;
}
