#include <iostream>
using namespace std;

// Функция сортировки массива
void sort (int* a, int size){
for (int i = 0; i < size; i++){
    for (int j = i+1; j <= size; j++){
        if ((a[j] < a[i])){
            int temp = a[j];
            a[j] = a[i]; a[i] = temp;
        }
    }
}
}

int main(){
    int* arr = new int[5];
    arr[0] = 2;
    arr[1] = 3;
    arr[2] = 1;
    arr[3] = 6;
    arr[4] = 5;

    sort(arr, sizeof(arr));
    for (int i = 0; i <sizeof(arr); i++){
        cout << arr[i] << " " << endl;
    }

}