#include <iostream>
using namespace std;

int function1 (int* a, int len){
int sum = 0;
for (int i = *a; i <= len; i++){
    sum = sum + i;
}
return sum;
}

int main(){
int a[5];
a[0] = 1;
a[1] = 2;
a[2] = 3;
a[3] = 4;
a[4] = 5;
int* a1 = &a[0];
int len = 5;
cout << len << endl;

int sum = function1(a1, len);
cout << sum << endl;
}

