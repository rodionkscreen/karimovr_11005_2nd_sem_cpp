#include <iostream>
using namespace std;

int main(){
    int len1 = 2;
    int len2 = 2;
    int* a1 = new int[len1];
    a1[0] = 1; a1[1] = 2;
    int* a2 = new int[len2];
    a2[0] = 1; a2[1] = 2;
    int* a3 = new int[len1 + len2];
    for (int i = 0; i < len1; i++){
        a3[i] = a1[i];
    }
    for (int i = 0; i < len2; i++){
        a3[i + len1] = a2[i];
    }
    for (int i = 0; i < sizeof(a3); i++){
        cout << a3[i];
    }
    a1 = 0;
    a2 = 0;
}